/* Déclaration des variables pour l'utilisation de canvas */

const canvas = document.getElementById('canvas');

const ctx = canvas.getContext('2d'); // Contexte de représentation bi-dimensionnel.


export { canvas, ctx };