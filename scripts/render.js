/* Importation de l'ensemble des scripts nécessaire à l'exécution du setup */

import { canvas, ctx } from "./canvas.js"; // Importation des paramètres canvas 2D
import { backgroundImage, blackBagImage, playerImage } from "./images.js"; // Importation des images du projet
import {
    globalSettings,
    speed,
    gravity,
    size,
    cTenth,
    bagWidth,
    bagGap,
  } from "./setting.js"; // Importation des constantes et variables nécessaires à l'exécution jeu
import { updateScore } from "./score.js";
import { setup } from "./setup.js";
import { addPoint , backgroundMusic } from "./sounds.js";


/* Déclaration d'une constante render au démarrage du jeu */

const render = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height); // Nettoyer les déplacements du personnage
    globalSettings.index++; // l'index prend une valeur de +1 à chaque boucle de chargement
  
    // On dessine deux fois le background en deux positions différentes pour avoir une impression de mouvement
    ctx.drawImage(
      backgroundImage,
      0,
      0,
      canvas.width,
      canvas.height,
      -((globalSettings.index * (speed / 2)) % canvas.width) + canvas.width,
      0,
      canvas.width,
      canvas.height
    );
    ctx.drawImage(
      backgroundImage,
      0,
      0,
      canvas.width,
      canvas.height,
      -((globalSettings.index * (speed / 2)) % canvas.width),
      0,
      canvas.width,
      canvas.height
    );
  
    // Si on est en train de jouer
    if (globalSettings.gamePlaying) {
      // On dessine Dolores
      ctx.drawImage(
        playerImage,
        Math.floor((globalSettings.index % 6) / 3) * size,
        0,
        size,
        size,
        cTenth,
        globalSettings.flyHeight,
        size,
        size
      ); // Placement en 1/10 sur l'axe horizontal selon les 3 positions
      globalSettings.flight += gravity;
      globalSettings.flyHeight = Math.min(globalSettings.flyHeight + globalSettings.flight, canvas.height - size); // On ne peut pas aller en dessous de l'écran de jeu
      globalSettings.flyHeight = Math.max(globalSettings.flyHeight + globalSettings.flight, 0); // On ne peut pas aller au dessus de l'écran de jeu
    } else {
      // Si on est sur l'écran principal du jeu
  
      // On dessine Dolores
      ctx.drawImage(
        playerImage,
        Math.floor((globalSettings.index % 6) / 3) * size,
        0,
        size,
        size,
        canvas.width / 2 - size / 2,
        globalSettings.flyHeight,
        size,
        size
      ); // Placement au milieu de l'écran selon les 3 positions
      globalSettings.flyHeight = canvas.height / 2 - size / 2;
  
      // On rajoute du texte sur l'écran
      ctx.fillText(`Meilleur score : ${globalSettings.bestScore}`, 280, 150);
      ctx.fillText("Cliquez pour jouer", 280, 170);
      ctx.font = "bold 18px courier";
      ctx.fillStyle = "white";
    }
  

    /* Apparition des bags (obstacles) lorsqu'on est entrain de jouer  */
  
    if (globalSettings.gamePlaying) {
        globalSettings.bags.map((bag) => {
        bag[0] -= speed; // Position en x des bags vaut -speed
        ctx.drawImage(
          blackBagImage,
          0,
          0,
          64,
          64,
          bag[0],
          bag[2],
          bagWidth,
          bag[1]
        ); // On dessine les bags selon l'array crée précédement
  
        // Si la position en x du bag sort de l'écran à gauche
        if (bag[0] <= -bagWidth) {
          globalSettings.currentScore++; // On rajoute +1 au score à chaque sac qui sort de l'écran
          addPoint.play();
          globalSettings.bestScore = Math.max(globalSettings.bestScore, globalSettings.currentScore); // On compare le best score et le current score pour garder le meilleur des deux
  
          globalSettings.bags = [
            ...globalSettings.bags.slice(1),
            [
              globalSettings.bags[globalSettings.bags.length - 1][0] + bagGap * bagWidth,
              bagWidth,
              Math.random() * (canvas.height - bagWidth - bagWidth / 2) +
                bagWidth / 2,
            ],
          ]; // A chaque sac qui sort de l'écran, on supprime le bag de l'array et on en rajoute un nouveau
        }
        // Si Dolores touche un sac => fin de la partie
        if (
          [
            bag[0] <= cTenth + size, // 1ère condition en x
            bag[0] + bagWidth >= cTenth, // 2ème condition en x
            bag[2] <= globalSettings.flyHeight + size / 2, // 1ère condition en y
            bag[2] + bagWidth >= globalSettings.flyHeight, // 2ème condition en y
          ].every((elem) => elem)
        ) {
          // On utilise .every pour lister plusieurs conditions à vérifier en même temps (pour chaque elem de elem)
          globalSettings.gamePlaying = false; // Si une condition est respectée => fin de la partie
          setup(); // On lance setup pour recharger le menu principal
        }
      });
    }
  

    /* On actualise les scores */

    updateScore();
};


/* Exportation du render */

export { render };
