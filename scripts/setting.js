/* Déclarations des variables globales du jeu */

const globalSettings = {
    gamePlaying : false, // boléeen renvoie true si on joue, false si on a perdu => menu principal
    index : 0, // l'index prend une valeur de +1 à chaque boucle de chargement
    bestScore : 0, // meilleur score
    currentScore : 0, // score actuel
    flight : 0, // distance de vol
    flyHeight : 0, // position en y du personnage
    bags : [], // création d'un array vide pour contenir les données des bags
}


/* Déclarations des constantes relatives à Dolores */

const speed = 6.2; // vitesse de défilement du décor
const gravity = 0.05; // force qui attire le personnage vers le bas de l'écran
const size = 31; // taille du personnage
const jump = -1.5; // force qui permet au personnage de remonter vers la surface
const cTenth = (canvas.width / 10); // le personnage est situé à 1/10ème de l'écran sur l'axe horizontal


/* Déclarations des constantes relatives aux sacs (obstacles) */

const bagWidth = 65; // largeur d'un sac
const bagGap = 6; // facteur de distance entre 2 sacs


/* Exportation des variables et constantes */

export { globalSettings, speed, gravity, size, jump, cTenth, bagWidth, bagGap };