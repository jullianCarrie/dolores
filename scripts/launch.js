/* Importation de l'ensemble des scripts nécessaire à l'exécution du launch */

import {
    globalSettings,
    jump,
  } from "./setting.js"; // Importation des constantes et variables nécessaires à l'exécution jeu
import { setup } from "./setup.js"; // Importation du setup
import { render } from "./render.js"; // Importation du render

import {backgroundMusic} from './sounds.js';

/* Lancement du jeu */

function launchGame() {
  setup(); // On lance le setup
  backgroundMusic.play();
  window.onload = render; // Au chargement de la fenêtre on lance render
  document.addEventListener("click", () => (globalSettings.gamePlaying = true)); // Au click de la souris on lance le jeu depuis l'écran principal
  window.onclick = () => (globalSettings.flight = jump); // Au click on attribue la valeur du jump à flight
}


/* Exportation de la fonction launch */

export { launchGame };