/* Importation de l'ensemble des scripts nécessaire à l'exécution du setup */

import { canvas } from "./canvas.js"; // Importation des paramètres canvas 2D
import {
    globalSettings,
    size,
    bagWidth,
    bagGap,
  } from "./setting.js"; // Importation des constantes et variables nécessaires à l'exécution jeu


/* Déclaration d'une constante setup lors du chargement du jeu */

const setup = () => {
    globalSettings.currentScore = 0; // Au chargement du jeu le score est de 0
    globalSettings.flyHeight = canvas.height / 2 - size / 2; // Position en y du personnage au milieu de l'écran
  
    // Création d'un mapping de 3 sacs lors du setup comprenant 3 arguments (array)
    globalSettings.bags = Array(3)
      .fill()
      .map((a, i) => [
        canvas.width + i * bagGap * bagWidth, // distance entre 2 sacs consécutifs
        bagWidth, // largeur d'un sac
        Math.random() * (canvas.height - bagWidth - bagWidth / 2) + bagWidth / 2, // position en y d'un sac déterminée de manière aléatoire dans l'écran de jeu
      ]);
  };


/* Exportation du setup */

export { setup };