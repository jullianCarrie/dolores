/* Importation de l'ensemble des scripts nécessaire à l'exécution de la modification des scores */

import {
    globalSettings,
  } from "./setting.js"; // Importation des constantes et variables nécessaires à l'exécution jeu
import { render } from "./render.js"


/* Actualisation des scores */

function updateScore() {
    document.getElementById("bestScore").innerHTML = `Meilleur : ${globalSettings.bestScore}`; // On ajoute du code html pour afficher le meilleur score
    document.getElementById(
        "currentScore"
    ).innerHTML = `Actuel : ${globalSettings.currentScore}`; // On ajoute du code html pour afficher le score actuel
    window.requestAnimationFrame(render); // On dit au navigateur de réaliser les animations en relançant render à chaque boucle
}


/* Exportation de la fonction pour modifier le score */

export { updateScore };
