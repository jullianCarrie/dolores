/* Importation des images du projet avec l'objet Image JS */

const backgroundImage = new Image();
backgroundImage.src = "./assets/img/background.png";

const blackBagImage = new Image();
blackBagImage.src = "./assets/img/blackbag.png";

const playerImage = new Image();
playerImage.src = "./assets/img/player.png";

export { backgroundImage, blackBagImage, playerImage };