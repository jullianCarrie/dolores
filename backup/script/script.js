window.onload = function () {
  let canvas = document.getElementById("canvas");
  let ctx = canvas.getContext("2d");

  let moveSpeedY = 0;

  let speedY = 0;
  let isMoving = false;

  function createPerso() {
    ctx.beginPath();
    ctx.arc(
      canvas.width / 10,
      canvas.height / 2 + moveSpeedY,
      10,
      0,
      Math.PI * 2
    );
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
  }

  function theGame() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    createPerso();
    
    if (isMoving === true) {
      console.log(isMoving);

      moveSpeedY += speedY;
    } else {
      speedY = 0;
    }
  }

  setInterval(theGame, 1);

  window.addEventListener("keydown", function (e) {
    if (e.key === "ArrowUp") {
      isMoving = true;
      speedY = -2;
    } else if (e.key === "ArrowDown") {
      isMoving = true;
      speedY = 2;
    }
  });
  window.addEventListener("keyup", function (e) {
    isMoving = false;
  });
};
